<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <?php
    session_start();
    $name = "";
    $connection = mysqli_connect("localhost", "root", "");
    $db = mysqli_select_db($connection, "sms");
    ?>
</head>

<body>
    <div id="header">
        <!-- <center> -->
        <div class="row">
            <div class="col-md-6">
                <h2 class="admin-dashboard-header">Student Management System</h2>
            </div>
            <div class="col-md-2">
                <div>Email: <?php echo $_SESSION['email']; ?></div>
            </div>
            <div class="col-md-2">
                <div>Name:<?php echo $_SESSION['name']; ?></div>
            </div>
            <div class="col-md-2">
                <a href="logout.php">Logout</a>
            </div>
        </div>
        <!-- </center> -->
    </div>
    <span id="top_span">
        <marquee>Note:- This portal is open till 31 March 2020...Plz edit your information, if wrong.</marquee>
    </span>
    <div class="row">
        <div class="col-md-3">
            <div id="sidebar">
                <form action="" method="post">
                    <h1 class="text-center py-5">Dashboard</h1>
                    <input type="submit" class="input-btn sidebar-items" name="search_student" value="Search Student"><br>
                    <input type="submit" class="input-btn sidebar-items" name="edit_student" value="Edit Student"><br>
                    <input type="submit" class="input-btn sidebar-items" name="add_new_student" value="Add New Student"><br>
                    <input type="submit" class="input-btn sidebar-items" name="delete_student" value="Delete Student"><br>
                    <input type="submit" class="input-btn sidebar-items" name="show teacher" value="Show Teachers"><br>
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div id="right_side">
                <div id="demo">
                    <!-- #Code for search student---Start-->
                    <?php
                    if (isset($_POST['search_student'])) {
                    ?>
                        <center>
                            <h4><b><u>Student's details</u></b></h4><br><br>
                            <form action="" method="post">
                                <b>Enter Roll No:</b> <input type="text" name="roll_no" placeholder="Search">
                                <input type="submit" name="search_by_roll_no_for_search" value="Search">
                            </form>
                        </center>

                        <center>
                            <table>
                                <tr>
                                    <td id="td"><b>Roll no.</b></td>
                                    <td id="td"><b>Name</b></td>
                                    <td id="td"><b>Class</b></td>
                                    <td id="td"><b>Phone No.</b></td>
                                    <td id="td"><b>Edit</b></td>
                                </tr>
                            </table>
                        </center>
                        <?php
                        $query = "select * from students";
                        $query_run = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($query_run)) {
                        ?>
                            <center>
                                <table>
                                    <tr>
                                        <form>
                                            <td id="td"><?php echo $row['roll_no'] ?></td>
                                            <td id="td"><?php echo $row['name'] ?></td>
                                            <td id="td"><?php echo $row['class'] ?></td>
                                            <td id="td"><?php echo $row['mobile'] ?></td>
                                        </form>
                                        <td><a class="btn btn-primary" data-toggle="modal" data-target="#editModal" href="edit_student.php?id=<?php echo $row['s_no']; ?>">Edit</a></td>
                                        <td><a class="btn btn-danger" href="delete_student.php?id=<?php echo $row['s_no']; ?>">Delete</a></td>
                                    </tr>
                                </table>
                            </center>
                        <?php
                        }
                        ?>

                        <?php
                    }
                    if (isset($_POST['search_by_roll_no_for_search'])) {
                        $query = "select * from students where roll_no = '$_POST[roll_no]'";
                        $query_run = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($query_run)) {
                        ?>
                            <table>
                                <tr>
                                    <td>
                                        <b>Roll No:</b>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $row['roll_no'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Name:</b>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $row['name'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Guardian's Name:</b>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $row['guardians_name'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Class:</b>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $row['class'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Mobile:</b>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $row['mobile'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Email:</b>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $row['email'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Password:</b>
                                    </td>
                                    <td>
                                        <input type="password" value="<?php echo $row['password'] ?>" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Remark:</b>
                                    </td>
                                    <td>
                                        <textarea rows="3" cols="40" disabled><?php echo $row['remark'] ?></textarea>
                                    </td>
                                </tr>
                            </table>
                    <?php
                        }
                    }
                    ?>
                    <!-- #Code for edit student details---Start-->
                    <?php
                    if (isset($_POST['edit_student'])) {
                    ?>
                        <center>
                            <form action="" method="post">
                                &nbsp;&nbsp;<b>Enter Roll No:</b>&nbsp;&nbsp; <input type="text" name="roll_no">
                                <input type="submit" name="search_by_roll_no_for_edit" value="Search">
                            </form><br><br>
                            <h4><b><u>Student's details</u></b></h4><br><br>
                        </center>
                        <?php
                    }
                    if (isset($_POST['search_by_roll_no_for_edit'])) {
                        $myvalue = $_GET['id'];
                        $query = "select * from students";
                        $query_run = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($query_run)) {
                        ?>
                            <form action="admin_edit_student.php" method="post">
                                <table>
                                    <tr>
                                        <td>
                                            <b>Roll No:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="roll_no" value="<?php echo $row['roll_no'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Name:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="name" value="<?php echo $row['name'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Gusrdian's Name:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="father_name" value="<?php echo $row['guardians_name'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Class:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="class" value="<?php echo $row['class'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mobile:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="mobile" value="<?php echo $row['mobile'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Email:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="email" value="<?php echo $row['email'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Password:</b>
                                        </td>
                                        <td>
                                            <input type="password" name="password" value="<?php echo $row['password'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Remark:</b>
                                        </td>
                                        <td>
                                            <textarea rows="3" cols="40" name="remark"><?php echo $row['remark'] ?></textarea>
                                        </td>
                                    </tr><br>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="submit" name="edit" value="Save">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                    <?php
                        }
                    }
                    ?>
                    <!-- #Code for Delete student details---Start-->
                    <?php
                    if (isset($_POST['delete_student'])) {
                    ?>
                        <center>
                            <form action="delete_student.php" method="post">
                                &nbsp;&nbsp;<b>Enter Roll No:</b>&nbsp;&nbsp; <input type="text" name="roll_no">
                                <input type="submit" name="search_by_roll_no_for_delete" value="Delete">
                            </form><br><br>
                        </center>
                    <?php
                    }
                    ?>

                    <?php
                    if (isset($_POST['add_new_student'])) {
                    ?>
                        <center>
                            <h4>Fill the given details</h4>
                        </center>
                        <form action="add_student.php" method="post">
                            <table>
                                <tr>
                                    <td>
                                        <b>Roll No:</b>
                                    </td>
                                    <td>
                                        <input type="text" name="roll_no" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Name:</b>
                                    </td>
                                    <td>
                                        <input type="text" name="name" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Guardian's Name:</b>
                                    </td>
                                    <td>
                                        <input type="text" name="guardians_name" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Class:</b>
                                    </td>
                                    <td>
                                        <input type="text" name="class" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Mobile:</b>
                                    </td>
                                    <td>
                                        <input type="text" name="mobile" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Email:</b>
                                    </td>
                                    <td>
                                        <input type="text" name="email" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Password:</b>
                                    </td>
                                    <td>
                                        <input type="password" name="password" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Remark:</b>
                                    </td>
                                    <td>
                                        <textarea rows="3" cols="40" placeholder="Optional" name="remark"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><br><input type="submit" name="add" value="Add Student"></td>
                                </tr>
                            </table>
                        </form>
                    <?php
                    }
                    ?>
                    <?php
                    if (isset($_POST['show_teacher'])) {
                    ?>
                        <center>
                            <h5>Teacher's Details</h5>
                            <table>
                                <tr>
                                    <td id="td"><b>ID</b></td>
                                    <td id="td"><b>Name</b></td>
                                    <td id="td"><b>Mobile</b></td>
                                    <td id="td"><b>Courses</b></td>
                                    <td id="td"><b>View Detail</b></td>
                                </tr>
                            </table>
                        </center>
                        <?php
                        $query = "select * from teachers";
                        $query_run = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($query_run)) {
                        ?>
                            <center>
                                <table style="border-collapse: collapse;border: 1px solid black;">
                                    <tr>
                                        <td id="td"><?php echo $row['t_id'] ?></td>
                                        <td id="td"><?php echo $row['name'] ?></td>
                                        <td id="td"><?php echo $row['mobile'] ?></td>
                                        <td id="td"><?php echo $row['courses'] ?></td>
                                        <td id="td"><a href="#">View</a></td>
                                    </tr>
                                </table>
                            </center>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <?php
                        $myvalue = $_GET['id'];
                        $query = "select * from students where s_no = '$myvalue'";
                        $query_run = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($query_run)) {
                        ?>
                            <form action="admin_edit_student.php" method="post">
                                <table>
                                    <tr>
                                        <td>
                                            <b>Roll No:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="roll_no" value="<?php echo $row['roll_no'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Name:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="name" value="<?php echo $row['name'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Gusrdian's Name:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="father_name" value="<?php echo $row['guardians_name'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Class:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="class" value="<?php echo $row['class'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mobile:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="mobile" value="<?php echo $row['mobile'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Email:</b>
                                        </td>
                                        <td>
                                            <input type="text" name="email" value="<?php echo $row['email'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Password:</b>
                                        </td>
                                        <td>
                                            <input type="password" name="password" value="<?php echo $row['password'] ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Remark:</b>
                                        </td>
                                        <td>
                                            <textarea rows="3" cols="40" name="remark"><?php echo $row['remark'] ?></textarea>
                                        </td>
                                    </tr><br>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="submit" name="edit" value="Save">
                                        </td>
                                    </tr>
                                </table>
                            </form>                            
                            <?php
                        }
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>